import React from "react";

export default function StyleFacet({ handleFilterClick, productStyle }) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productStyle = sortObject(productStyle);

  return (
    <div class="facet-wrap facet-display">
      <strong>Style</strong>
      <div className="facetwp-facet">
        {Object.keys(productStyle).map((style, i) => {
          if (style && productStyle[style] > 0) {
            return (
              <div>
                <span
                  id={`style-filter-${i}`}
                  key={i}
                  data-value={`${style.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("style_facet", e.target.dataset.value)
                  }>
                  {" "}
                  {style} {` (${productStyle[style]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
